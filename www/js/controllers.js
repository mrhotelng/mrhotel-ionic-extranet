angular.module('starter.controllers', [])


    .controller('inventoryDetail', function($scope,HotelInventory,$stateParams,$ionicLoading,$ionicPopup) {
       $scope.details = HotelInventory.getCurrentAvailDetails();
       $scope.repeatOptions = HotelInventory.getRepeatOptions();
       $scope.roomDetails = HotelInventory.getCurrentRoom();

        $scope.startLoad = function(){
            $ionicLoading.show({
                template:"Loading",
                duration:5000
            })
        }
        $scope.finishLoad = function(){
            $ionicLoading.hide();
        }

        $scope.saveInventory = function (){
            $scope.startLoad();
        }

        $scope.closeInventory = function() {
            var confirmPopup = $ionicPopup.confirm({
                title: 'Close Out',
                template: 'Are you sure you want to close out this sale?'
            });
            confirmPopup.then(function(res) {
                if(res) {
                    console.log('You are sure');
                    //TODO connect to the manager to send a delete request to the server with the ID of the inventory
                } else {

                    console.log('Returned to previous state');
                }
            });
        };


       console.log($scope.details)

    })



.controller('profileCtrl', function($scope,UserProfile,HotelInventory) {

  $scope.editMode = false;
  $scope.profileEdit = {};
  $scope.profile = {};

  $scope.enterEditMode = function(){
      //extend the old variable into the new one
      $scope.profileEdit= angular.copy($scope.profile);
      $scope.editMode = true;

  }
  $scope.cancelEdit = function(){
      $scope.editMode = false;
  }

  $scope.saveProfile = function(){
      $scope.profile =  angular.copy($scope.profileEdit);
      //push the object to the server
      HotelInventory.updateHotelInformation($scope.profile,UserProfile.getUserAccount().hotel_id
      ,function(err,result){
              $scope.editMode = false;
              if(err){
                HotelInventory.showFlashMessage('Error','Error saving the hotel information');

                return;
              }
              HotelInventory.showAlert('Success','Profile Saved!');
          })

  }

  HotelInventory.getHotelInformation(UserProfile.getUserAccount().hotel_id,function(err,result){
      if(err){
          HotelInventory.showAlert('Error','Could not get hotel profile from server') ;
          return;
      }
      console.log(result);
      $scope.profile = result;

  })
})



.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
})




.controller('AccountCtrl', function($scope,HotelInventory,UserProfile,$ionicModal) {
    $scope.logs = null
    $scope.currentItem = null;
        HotelInventory.getHotelLogs(UserProfile.getUserAccount().hotel_id,function(err,result){
           if(err){
               HotelInventory.showAlert('Error','Server cannot be reached')
               return;
           }
           $scope.logs = result;

        })



        $ionicModal.fromTemplateUrl('templates/modals/logInformation.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
                $scope.modal = modal

            });
        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };

        $scope.openLogInfoModal = function(item){
            $scope.currentItem  = item;
            $scope.modal.show();
        }


  $scope.settings = {
    enableFriends: true
  };
})





.controller('loginController',function($scope,UserProfile,TokenService,$ionicLoading,$ionicPopup,$location,$cordovaDevice,$ionicPlatform,$cordovaPush,$rootScope,$http){

    $scope.loginCred = {};

    $scope.login = function(){

        $scope.startLoad = function(){
            $ionicLoading.show({
                template:"Loading"
            })
        }
        $scope.finishLoad = function(){
            $ionicLoading.hide();
        }


           var obj = {}
           obj.username = $scope.loginCred.email;
           obj.password = $scope.loginCred.password;

           $scope.startLoad();
           $http.post(SERVER+'/user/auth',angular.toJson(obj)).
               success(function(data,status){
                   //if successful then get the application with GCM and push the token to the server
                   //set the user profile
                   //console.log(data)
                   UserProfile.setUserAccount(data);
                   console.log(UserProfile.getUserAccount())
                   TokenService.setToken(data.token);
                   $ionicLoading.hide();

                   for(var i=0; i<500000;i++){

                   }
                   $location.path('/tab/dash');

                  /* $ionicPlatform.ready(function() {
                       var device = $cordovaDevice.getPlatform();
                       console.log(device);
                       //register with the service needed
                       if(device=='Android'){
                           var androidConfig = {
                               "senderID": "470999926099"
                           };
                           $cordovaPush.register(androidConfig).then(function(result) {
                               $rootScope.$on('pushNotificationReceived', function(event, notification) {
                                   switch(notification.event) {
                                       case 'registered':
                                           if (notification.regid.length > 0 ) {
                                               // Simple POST request example (passing data) :
                                               var userToken;
                                               $http.post(SERVER+'/user/registerDeviceToken',{devicePushToken:notification.regid,userToken:TokenService.getUserToken(),deviceType:device.toLocaleLowerCase()}).
                                                   success(function(data, status, headers, config) {
                                                       UserProfile.setUserAccount(data);
                                                       $ionicLoading.hide();
                                                       $location.path('/tab/dash');
                                                       return;
                                                   }).
                                                   error(function(data, status, headers, config){
                                                       $ionicLoading.hide();
                                                       alert("Cannot register device for push notification");
                                                       $location.path('/tab/dash');
                                                       return;
                                                   });
                                           }
                                           break;
                                       case 'message':
                                           // this is the actual push notification. its format depends on the data model from the push server
                                           alert('message = ' + notification.message + ' msgCount = ' + notification.msgcnt);
                                           break;

                                       case 'error':
                                           alert('GCM error = ' + notification.msg);
                                           break;

                                       default:
                                           alert('An unknown GCM event has occurred');
                                           break;
                                   }
                               });

                           }, function(err) {
                               $ionicLoading.hide();
                               alert(err);
                           });
                       }
                   }); */

               }).error(function(data,status){
                   $ionicLoading.hide();
                   HotelInventory.showAlert('Error','Cannot sign you in.')
               })


    }


})

    .controller('DashCtrl',function($scope,UserProfile,HotelInventory,$ionicModal,$ionicLoading,$location) {
        $scope.modal = null;
        $scope.inventoryByDate = null
        $ionicModal.fromTemplateUrl('templates/modals/filterListing.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
                $scope.modal = modal

            });
        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };

        $scope.loadFilterModal = function(){
            $scope.modal.show();
        }
        $scope.isDateToday = function(theDate){
            var rightNow = new Date()
            var todayDate = rightNow.getDate();
            var todayMonth = rightNow.getMonth();
            var todayYear = rightNow.getFullYear();
            if(todayDate==theDate.getDate()&&todayMonth==theDate.getMonth()&&todayYear==theDate.getFullYear()) {
                return "Tonight"

            }
            else if((todayDate+1)==(theDate.getDate())&&todayMonth==theDate.getMonth()&&todayYear==theDate.getFullYear()){
                return "Tomorrow"
            }
            return false;
        }
        $scope.lowerThanToday = function(theDate){
            if($scope.isDateToday(theDate)){
               return false;
            }
            if(theDate<new Date()){
                return true;
            }
            return false;

        }

        $scope.getImageLink = function(imageString){
            var imageObj = JSON.parse(imageString);
            return imageObj.url;
        }

        $scope.getDate=function(date){
            return new Date(date);
        }

        $scope.setCurrentDate = function(item){
            HotelInventory.setCurrentDate(item,function(){
                 $location.path('/tab/dash/dateSummary');
            });
        }

        console.log(UserProfile.getUserAccount());
        $scope.init = function(callback){
            HotelInventory.loadInventoryDetails(UserProfile.getUserAccount().hotel_id,function(err,result){
                if(err){
                    HotelInventory.showAlert('Error','Inventory details cannot be loaded')
                    return;
                }
                for(var i=0;i<result.length;i++){
                    result[i].date = new Date(result[i].date);
                }
                $scope.inventoryByDate = result;
                callback();
            })
        }
        $scope.init(function(){
            //TODO - loading spinner should be made to disappear here
        });
        $scope.doRefresh = function() {
            $scope.init(function(){
                $scope.$broadcast('scroll.refreshComplete');
                HotelInventory.showFlashMessage('Success','Refresh Completed!')
            })

        };

        //openloading thing here and call web service


        /*$scope.hotelRoomTypes = UserProfile.getRoomTypes();
        $scope.roomFilter = {}
        $scope.roomFilter.roomType = $scope.hotelRoomTypes[0]
        //$scope.inventoryByDate = HotelInventory.getInventoryList();  */

    })

    .controller('HotelAvailabilityCntrl', function($scope,HotelInventory,$stateParams,$location) {
        var room_id = $stateParams.id;
        $scope.roomDetails = HotelInventory.getCurrentDate();
        console.log($scope.roomDetails);


          $scope.gotoDetails = function(item){
              if(new Date() > new Date(item.available_date)){
                  HotelInventory.showFlashMessage('Error','This data is not available anymore here')

                  return;
              }
              HotelInventory.setCurrentAvailDetails(item,function(){
                  $location.path('/tab/dash/details/'+item.room_id);
              });
          }

        //calculate all the discounts
        for(var i=0;i<$scope.roomDetails.hotels.length;i++){
            var diff = $scope.roomDetails.hotels[i].original_price - $scope.roomDetails.hotels[i].checkin_day_price;
            $scope.roomDetails.hotels[i].originalPercentageOff = parseInt(((diff/$scope.roomDetails.hotels[i].original_price)*100));


            var diff2 = $scope.roomDetails.hotels[i].original_price - $scope.roomDetails.hotels[i].remaining_day_price;
            $scope.roomDetails.hotels[i].remainingPercentageOff = parseInt(((diff/$scope.roomDetails.hotels[i].original_price)*100));



        }

        $scope.getImageLink = function(imageString){
            var imageObj = JSON.parse(imageString);
            return imageObj.url;
        }
        $scope.originalDayChange = function(index){

            if($scope.roomDetails.hotels[index].originalPercentageOff>0 && $scope.roomDetails.hotels[index].originalPercentageOff<=100){
                //calculate the new discount
                var discount = parseInt(($scope.roomDetails.hotels[index].originalPercentageOff/100)* $scope.roomDetails.hotels[index].original_price);
                $scope.roomDetails.hotels[index].checkin_day_price =  $scope.roomDetails.hotels[index].original_price-discount;
                return;
            }
            $scope.roomDetails.hotels[index].checkin_day_price = $scope.roomDetails.hotels[index].original_price
        }
        $scope.remainingDayChange = function(index){
            if($scope.roomDetails.hotels[index].remainingPercentageOff>0 && $scope.roomDetails.hotels[index].remainingPercentageOff<=100){
                //calculate the new discount
                var discount = parseInt(($scope.roomDetails.hotels[index].remainingPercentageOff/100)* $scope.roomDetails.hotels[index].original_price);
                $scope.roomDetails.hotels[index].checkin_day_price =  $scope.roomDetails.hotels[index].original_price-discount;
                return;
            }
            $scope.roomDetails.hotels[index].remaining_day_price = $scope.roomDetails.hotels[index].original_price

        }

        $scope.saveAllRooms = function(){
             HotelInventory.saveAllRooms($scope.roomDetails.hotels,function(err,data){
                 if(err){
                     HotelInventory.showFlashMessage('Error','Error Saving the rooms')
                     return
                 }
                 HotelInventory.showFlashMessage('Success','Saved!')


             })
        }
    })

    .controller('bookingCtrl', function($scope, Chats,Bookings,HotelInventory,UserProfile,$ionicLoading) {
        $scope.chats = Chats.all();
        $scope.hotelRooms = {}
        HotelInventory.reloadInventoryList(UserProfile.getUserAccount().hotel_id)
        setTimeout(function(){ $scope.hotelRooms = HotelInventory.getInventorhotelIdyList()
            $ionicLoading.hide();console.log($scope.inventoryByDate)}, 2000);

        $scope.bookings = Bookings.getHotelBookings();

        $scope.remove = function(chat) {
            Chats.remove(chat);
        }

        $scope.getImageLink = function(imageString){
            var imageObj = JSON.parse(imageString);
            return imageObj.url;
        }

        $scope.setCurrentBooking = function(booking){
            Bookings.setCurrentBooking(booking);
        }
    })

    .controller('BookingDetailCtrl', function($scope, $stateParams, Chats,HotelInventory,UserProfile) {
        $scope.bookingDetail = HotelInventory.getBookingDetail();
        $scope.removeUnderScore = function(data){
           return data.replace("_"," ");
        }

        console.log($scope.bookingDetail);

    })

.controller('roomBookingContrl',function($scope, Chats,Bookings,HotelInventory,UserProfile,$ionicLoading,$stateParams,$location){
        $scope.roomBookings = {}
        HotelInventory.getRoomBookings($stateParams.roomId,function(err,result){
            if(err){
                HotelInventory.showFlashMessage('Error','cannot get room bookings from server')
                return;
            }
            $scope.roomBookings = result;

        });

        $scope.gotoBookingDetails = function(item){
               HotelInventory.setBookingDetail(item,function(){
                    $location.path('/tab/booking/'+item.id);

               });
        }
    });

