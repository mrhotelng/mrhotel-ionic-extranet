angular.module('starter.services', [])

.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Ben Sparrow',
    lastText: 'You on your way?',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    lastText: 'Hey, it\'s me',
    face: 'https://pbs.twimg.com/profile_images/479740132258361344/KaYdH9hE.jpeg'
  }, {
    id: 2,
    name: 'Andrew Jostlin',
    lastText: 'Did you get the ice cream?',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    lastText: 'I should buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    lastText: 'Look at my mukluks!',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  }
})

/**
 * A simple example service that returns some data.
 */
.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  // Some fake testing data
  var friends = [{
    id: 0,
    name: 'Ben Sparrow',
    notes: 'Enjoys drawing things',
    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
  }, {
    id: 1,
    name: 'Max Lynx',
    notes: 'Odd obsession with everything',
    face: 'https://pbs.twimg.com/profile_images/479740132258361344/KaYdH9hE.jpeg'
  }, {
    id: 2,
    name: 'Andrew Jostlen',
    notes: 'Wears a sweet leather Jacket. I\'m a bit jealous',
    face: 'https://pbs.twimg.com/profile_images/491274378181488640/Tti0fFVJ.jpeg'
  }, {
    id: 3,
    name: 'Adam Bradleyson',
    notes: 'I think he needs to buy a boat',
    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
  }, {
    id: 4,
    name: 'Perry Governor',
    notes: 'Just the nicest guy',
    face: 'https://pbs.twimg.com/profile_images/491995398135767040/ie2Z_V6e.jpeg'
  }];


  return {
    all: function() {
      return friends;
    },
    get: function(friendId) {
      // Simple index lookup
      return friends[friendId];
    }
  }
})
.factory('UserProfile',function(){
   var roomTypes = ['All','Single','Double','Suite','Presendential'] //TODO - wire this to an endpoint to populate this array later on Production will only have all
   var userAccount = {}
        return {
            setUserAccount:function(account){
                userAccount = account
            },
            getUserAccount:function(){
                return userAccount
            },
            getRoomTypes: function() {
                return roomTypes;
            }

        }

 }).factory('HotelInventory',function(UserProfile,$http,$ionicPopup){
        var repeatOptions = ['Today','Daily','Weekends', 'WeekDays'] ;
        var CurrentInventory = {};
        var currentRoom = {};
        var bookingDetail;
        var currentDate = {};
        var inventoryDates = [
            {   id:1,
                date:"December 26, 2014",
                numberSold:8,
                numberAvailable:20,
                percentageAvailable:20
            },{
                id:2,
                date:"December 27, 2014",
                numberSold:8,
                numberAvailable:20,
                percentageAvailable:15
            },{
                id:3,
                date:"December 28, 2014",
                numberSold:8,
                numberAvailable:20,
                percentageAvailable:15
            }

        ]
        var inventoryDetails =[
            {
                id:0,
                roomType:"Standard",
                standardPrice:16000,
                emptyRooms:10,
                soldRooms:5,
                sameDayDiscount:20,
                remainingNightRate:16000,
                repeat:"Today"


            },
            {
                id:1,
                roomType:"Deluxe",
                standardPrice:20000,
                emptyRooms:20,
                soldRooms:10,
                sameDayDiscount:10,
                remainingNightRate:13000,
                repeat:"Daily"


            } ,{
                id:2,
                roomType:"King",
                standardPrice:50000,
                emptyRooms:0,
                soldRooms:0,
                sameDayDiscount:null,
                remainingNightRate:null,
                repeat:null
            }


        ]
        return {
            setCurrentDate:function(item,callback){
              currentDate = item;
              callback();
            },
            getCurrentDate :function(){
              return currentDate;
            },

            setCurrentRoom:function(item,callback){
                currentRoom = item;
                callback();

            },
            getCurrentRoom:function(){
                return currentRoom;
            } ,

            setCurrentAvailDetails:function(item,callback){
                CurrentInventory = item;
                callback()
            },
            getCurrentAvailDetails:function(){
                return CurrentInventory;
            },
            setBookingDetail:function(item,callback){
                bookingDetail = item;
                callback();
            },
            getBookingDetail:function(){
                return bookingDetail;
            } ,

            reloadInventoryList: function(hotelId) {
                var isWebStillWorking = true;
                var error = null
                //run the http service here to get the latest poll from the server
                $http.get(SERVER+'/hotel/getInventory/'+hotelId)
                    .success(function(data, status){
                        inventoryDates = data;
                        return

                    }).error(function(data,status){
                        alert("Unable to refresh inventory list from server")
                        return;
                    })

            } ,
            getRoomAvailability:function(id,callback){
                $http.get(SERVER+'/hotel/getRoomAvailability/'+id)
                    .success(function(data,status){
                        callback(null,data);
                    })
                    .error(function(data,status){
                        callback(status,null)
                    })
            },
            getDetailForRoomInventory:function(id){
                for (var i = 0; i < inventoryDetails.length; i++) {
                    if (inventoryDetails[i].id === parseInt(id)) {
                        return inventoryDetails[i];
                    }
                }
                return null;
            },
            getOneInventory:function(id){
                for (var i = 0; i < inventoryDates.length; i++) {
                    if (inventoryDates[i].id === parseInt(id)) {
                        return inventoryDates[i];
                    }
                }
                return null;
            },
            getRepeatOptions : function(){
                return  repeatOptions;
            } ,
            getInventorhotelIdyList:function(){
                return inventoryDates;
            },
            getHotelLogs :function(hotel_id,callback){
                $http.get(SERVER+'/hotel/extranetLog/'+hotel_id)
                    .success(function(data,status){
                       callback(null,data);
                    })
                    .error(function(data,status){
                        callback(status,null);
                    })
            } ,
            getHotelInformation:function(hotel_id,callback){
                $http.get(SERVER+'/hotel/information/'+hotel_id)
                    .success(function(data,status){
                        callback(null,data);
                        return;

                    })
                    .error(function(data,status){
                        callback(status,null);
                    })


            } ,
            getRoomBookings:function(room_id,callback){
                $http.get(SERVER+'/hotel/getHotelRoomBookings/'+room_id)
                    .success(function(data,status){
                        callback(null,data);
                        return
                    })
                    .error(function(data,status){
                        callback(status,null);
                    })
            } ,
            loadInventoryDetails:function(hotel_id,callback){
                $http.get(SERVER+'/hotel/getAvailability/V2/'+hotel_id)
                    .success(function(data,status){
                        callback(null,data);
                    })
                    .error(function(data,status){
                        callback(status,null);
                    })


            } ,
            saveAllRooms:function(rooms,callback){
                $http.post(SERVER+'/hotel/updateInventory',angular.toJson(rooms))
                    .success(function(data,status){
                        callback(null,data);
                    })
                    .error(function(data,status){
                        callback(status,data);
                    })
            } ,
            updateHotelInformation:function(hotelDto,hotel_id,callback){
                $http.post(SERVER+'/hotel/updateInformation/'+hotel_id, angular.toJson(hotelDto))
                    .success(function(data,status){
                        callback(null,data);
                    })
                    .error(function(data,status){
                        callback(status,data);
                    })

            }
            ,showAlert:function(title,body){
                var alertPopup = $ionicPopup.alert({
                    title: title,
                    template:body
                });
                alertPopup.then(function(res) {

                });
            }
            ,showFlashMessage:function(title,body){
                $scope.data = {}

                // An elaborate, custom popup
                var myPopup = $ionicPopup.show({
                    template: body,
                    title: title

                });
                myPopup.then(function(res) {
                });
                $timeout(function() {
                    myPopup.close(); //close the popup after 3 seconds for some reason
                }, 3000);

            }
        }

 })
.factory('TokenService',function(){

     return {
         getUserToken:function(){
            return localStorage.getItem("vendorMobileToken");
         },
         checkToken : function(callback){
             if(!localStorage.getItem("vendorMobileToken")){
               callback(false)
             };
             callback(localStorage.getItem("vendorMobileToken"));
         },
         setToken :function(token){
            localStorage.setItem('vendorMobileToken',token);
         }
     }
})

.factory('Bookings',function(){
        var currentBooking = null;
        var hotelBookings = [
            {date:"10 October,2014",
             bookings:[{
                paymentID:'34995dhf9',
                 date:"10 October,2014",
                 name:'Megan Fora',
                 amountPaid:35000,
                 numberOfNights:2,
                 status:'check-in',
                 phone:'0807894561'

                },
                 {
                     paymentID:'34995dhf9',
                     date:"10 October,2014",
                     name:'John Smith',
                     amountPaid:35000,
                     numberOfNights:2,
                     status:'check-in',
                     phone:'0807894561'
                 },
                 {
                     paymentID:'34995dhf9',
                     date:"10 October,2014",
                     name:'Joyce Rice',
                     amountPaid:35000,
                     numberOfNights:2,
                     status:'check-in',
                     phone:'0807894561'
                 }]

            } ,
            {date:"11 October,2014",
                bookings:[{
                    paymentID:'34995dhf9',
                    date:"10 October,2014",
                    name:'Megan Good',
                    amountPaid:35000,
                    numberOfNights:2,
                    status:'check-in',
                    phone:'0807894561'
                },
                    {
                        paymentID:'34995dhf9',
                        date:"10 October,2014",
                        name:'Rita Lora',
                        amountPaid:35000,
                        numberOfNights:2,
                        status:'check-in',
                        phone:'0807894561'
                    },
                    {
                        paymentID:'34995dhf9',
                        date:"10 October,2014",
                        name:'Emeka Nwoke',
                        amountPaid:35000,
                        numberOfNights:2,
                        status:'check-in',
                        phone:'0807894561'
                    }]

            },
            {date:"12 October,2014",
                bookings:[{
                    paymentID:'34995dhf9',
                    date:"10 October,2014",
                    name:'Megan Fora',
                    amountPaid:35000,
                    numberOfNights:2,
                    status:'check-in',
                    phone:'0807894561'
                },
                    {
                        paymentID:'34995dhf9',
                        date:"10 October,2014",
                        name:'Megan Fora',
                        amountPaid:35000,
                        numberOfNights:2,
                        status:'check-in',
                        phone:'0807894561'
                    },
                    {
                        paymentID:'34995dhf9',
                        date:"10 October,2014",
                        name:'Megan Fora',
                        amountPaid:35000,
                        numberOfNights:2,
                        status:'check-in',
                        phone:'0807894561'
                    }]

            }]
        return {
            getHotelBookings: function(){
                return hotelBookings;
            } ,
            changeBookingStatus:function(){

            },
            setCurrentBooking:function(booking){
                currentBooking = booking;
            },
            getCurrentBooking:function(){
                return currentBooking;
            }


        }


    })
